
<?php 
class troco{
    public function retornaTroco($total, $valorRecebido){	
        $totalInt = intval($total);
        $valorRecebidoInt = intval($valorRecebido);
        if($totalInt > $valorRecebidoInt){
            print "Valor pago menor que valor total da compra";
        }else{
            $moedas1 = $valorRecebidoInt - $totalInt; 
            $valorRecebidoFrac = $valorRecebido - $valorRecebidoInt;
            $totalFrac = $total - $totalInt;
            if($moedas1 == 0 && $totalFrac > $valorRecebidoFrac){
                print "Valor pago menor que valor total da compra";
            }elseif($moedas1 == 0 && $valorRecebidoFrac > $totalFrac ) {
                $trocoFrac = $valorRecebidoFrac - $totalFrac;
                $retorno = $this->retornaMoedas($moedas1,$trocoFrac);
            }elseif($valorRecebidoFrac > $totalFrac){
                $retorno = $this->retornaMoedas($moedas1,$trocoFrac);
                print "$retorno";
            }elseif($totalFrac > $valorRecebidoFrac){
                $trocoFrac = $valorRecebidoFrac + 1;
                
                $trocoFrac -= $totalFrac;
                $moedas1--;
                $retorno = $this->retornoMoedas($moedas1, $trocoFrac);
                print "$retorno";
            }elseif($moedas1 == 0 && $totalFrac == $valorRecebidoFrac){
                print "Não há moedas de troco";
            }
        }
        
    }


    public function retornaMoedas($moedas1,$trocoFrac){
        while($trocoFrac > 24){
            $trocoFrac -= 0.25;
            $moedas25++;
        }
        if($trocoFrac > 0){
            while($trocoFrac > 9){
                $trocoFrac -= 0.10;
                $moedas10++;
            }
        }else{
            return "$moedas1 moedas de 1 real e $moedas25 moedas de 0.25";
        }
        if($trocoFrac > 0){
            while($trocoFrac > 4){
                $trocoFrac -= 0.05;
                $moedas05++;
            }
        }else{
            return "$moedas1 moedas de 1 real , $moedas25 moedas de 0.25 e $moedas10 moedas de 0.10";
        }
        if($trocoFrac > 0){
            $moedas01 = $trocoFrac;
            return "$moedas1 moedas de 1 real , $moedas25 moedas de 0.25 , $moedas10 moedas de 0.10 , $moedas05 moedas de 0.05 e $moedas01 moedas de 0.01";
        }else{
            return "$moedas1 moedas de 1 real , $moedas25 moedas de 0.25 , $moedas10 moedas de 0.10 e $moedas05 moedas de 0.05";
        }
    }
}

?>